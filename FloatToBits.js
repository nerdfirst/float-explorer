var FloatToBits = {
    "go": function() {
        var fieldText = document.getElementById("numInput").value;
        var val = parseFloat(fieldText);

        if (isNaN(val)) {
            document.getElementById("numToBitsError").style.display = "block";
            document.getElementById("numToBitsResult").style.display = "none";
            return;
        }
        else {
            document.getElementById("numToBitsError").style.display = "none";
            document.getElementById("numToBitsResult").style.display = "block";
        }

        var integerPortion = val<0? Math.ceil(val) : Math.floor(val);
        var decimalPortion = DecimalTools.extractDecimals(fieldText);

        var results = FloatToBits.convertToBinary(integerPortion, decimalPortion);
        FloatToBits.doBinaryWorking(results);
    },

    "convertToBinary": function(integer, decimal) {
        var usefulBits = 0;
        var sign = 1;

        if (integer < 0) {
            sign = -1;
            integer *= -1;
        }

        var intTable = document.getElementById("integerPortionWorking");
        intTable.innerHTML = "<tr><td colspan=4 style='text-align:center; font-weight:bold'>Integer Portion</td></tr>";

        var intBits = [];
        while (integer > 0) {
            var elem = "<tr><td>2</td><td class='division'>" + integer + "</td><td style='font-weight:bold'>" + integer%2 + "</td></tr>";
            intTable.innerHTML += elem;

            usefulBits++;
            intBits.unshift(integer % 2);
            integer = Math.floor(integer / 2);
        }

        var floatTable = document.getElementById("decimalPortionWorking");
        floatTable.innerHTML = "<tr><td colspan=4 style='text-align:center; font-weight:bold'>Decimal Portion</td></tr>";

        var floatBits = [];
        while (DecimalTools.greaterThanZero(decimal) && usefulBits < 23) {
            var doubled = DecimalTools.double(decimal);
            var result = DecimalTools.greaterThanOrEqualOne(doubled)? 1:0;
            
            if (usefulBits != 0 || result == 1) {
                usefulBits++;
            }

            floatBits.push(result);

            var elem = "<tr><td class='multiplicationLeft'>" + decimal + " x 2</td><td>=</td><td>" + doubled + "</td><td class='multiplicationRight'>" + result + "</td></tr>";
            floatTable.innerHTML += elem;

            if (DecimalTools.greaterThanOrEqualOne(doubled)) {
                decimal = DecimalTools.subtractOne(doubled);
            }
            else {
                decimal = doubled;
            }
        }

        var truncated = false;
        if (DecimalTools.greaterThanZero(decimal)) {
            var dots = "<tr><td colspan=4 style='font-weight:bold; text-align:center'> : </td></tr>";
            floatTable.innerHTML += dots + dots;
            truncated = true;
        }

        return {"intBits":intBits, "floatBits":floatBits, "sign":sign, "truncated":truncated};
    },

    "doBinaryWorking": function(data) {
        var integerBits = data.intBits;
        var floatBits = data.floatBits;
        
        var sign = data.sign;
        var exponent = 0;
        var mantissa = "";

        var isDenormal = false;
        var hasOverflowed = false;

        var signSymbol = sign>0? "":"-";
        var truncatedText = data.truncated? " (truncated)":"";
        var result = signSymbol + integerBits.join("") + "." + floatBits.join("");

        if (floatBits.length == 0 && integerBits.length == 0) {
            result = "0"
            sign = 1;
            exponent = -127;
            mantissa = "00000000000000000000000";
        }
        else {
            if (floatBits.length == 0) {
                result += "0";
            }
            result += truncatedText + "<br>"

            if (integerBits.length == 0) {
                result = signSymbol + "0" + result;
                while (floatBits[0] != 1 && exponent > -126) {
                    floatBits.shift();
                    exponent -= 1;
                }

                if (floatBits[0] == 1 && exponent >= -125) {
                    // Normal number
                    exponent -= 1;
                    isDenormal = false;
                }
                else if (floatBits[0] == 1 && exponent == -126) {
                    // Edge case: Number with exponent -127
                    floatBits.unshift(0);
                }
				
				isDenormal = floatBits[0] == 0;

                result += " = " + signSymbol + floatBits[0] + "." + floatBits.join("").slice(1);
                if (floatBits.length == 1) {
                    result += "0";
                }
                result += " x 2<span class='superscript'>" + exponent + "</span>";

                mantissa = floatBits.join("").slice(1);
            }
            else {
                exponent = integerBits.length - 1;

                if (exponent > 126) {
                    hasOverflowed = true;
                    exponent = 127;
                }

                result += " = " + signSymbol + integerBits[0] + "." + integerBits.join("").slice(1) + floatBits.join("");
                result += " x 2<span class='superscript'>" + exponent + "</span>";
                mantissa = integerBits.join("").slice(1) + floatBits.join("");
            }
        }

        document.getElementById("computedSignBit").innerHTML = sign>0? 0:1;
        document.getElementById("ansSignSpan").innerHTML = sign>0? 0:1;

        var mantissaWarning = "";
        mantissa = mantissa.substring(0,23);
        if (hasOverflowed) {
            mantissa = "11111111111111111111111"
            mantissaWarning = " (overflow)"
        }
        document.getElementById("computedMantissaBits").innerHTML = mantissa + mantissaWarning; 

        while (mantissa.length < 23) {
            mantissa = mantissa + "0";
        }

        mantissa = mantissa.slice(0,7) + " " + mantissa.slice(7,15) + " " + mantissa.slice(15);
        document.getElementById("ansMantissaSpan").innerHTML = mantissa;

        var biasedExponent = exponent + 127
        var exponentBits = []
        while (biasedExponent > 0 && !isDenormal) {
            var bit = biasedExponent % 2 == 0? 0:1;
            exponentBits.unshift(bit);

            if (bit == 1) biasedExponent--;
            biasedExponent /= 2;
        }
        
        if (exponentBits.length == 0) {
            exponentBits.push("0")
        }

        if (isDenormal) {
            document.getElementById("computedExponentBits").innerHTML = "= -126(d) <br>Expressed as 0(b) (denormal number)"
        }  
        else {
            document.getElementById("computedExponentBits").innerHTML = exponent + " + 127 = " + (exponent+127) + "(d)<br> = " + exponentBits.join("") + "(b)";
        }

        while (exponentBits.length < 8) {
            exponentBits.unshift("0")
        }

        document.getElementById("ansExpSpan").innerHTML = exponentBits.join("");
        
        document.getElementById("decimalStringSpan").innerHTML = result;

        var finalString = "" + (sign>0?0:1) + exponentBits.join("") + mantissa;
        finalString = finalString.slice(0,8) + " " + finalString.slice(8);
        document.getElementById("bitsInput").value = finalString;

        if (isDenormal) {
            document.getElementById("computedMantissaBits").innerHTML += " (denormal number)";
            document.getElementById("decimalStringSpan").innerHTML += " (denormal number)";
        }

        BitsToFloat.go();
    }
}


DecimalTools = {
    "extractDecimals": function(val) {        
        var stringVersion = "" + val;
        var pointPosition = stringVersion.indexOf(".");

        if (pointPosition == -1) {
            return "";
        }

        return "0." + stringVersion.slice(pointPosition+1);
    },

    "double": function(val) {
        var result = "";

        var carry = 0;
        for (var i=val.length-1; i>=0; i--) {
            var c = val.charAt(i);
            if (c == ".") {
                result = "." + result;
                continue;
            }

            var num = parseInt(c) * 2 + carry;

            if (num < 10) {
                carry = 0;
                result = "" + num + result;
            }
            else {
                carry = 1;
                result = "" + (num - 10) + result;
            }
        }

        return result;
    },

    "greaterThanOrEqualOne": function(val) {
        return val.charAt(0) != "0";
    },

    "greaterThanZero": function(val) {
        return parseFloat(val) > 0;
    },

    "subtractOne": function(val) {
        return "0" + val.slice(1);
    }
}