var BitsToFloat = {
    "replaceAll": function(text, item, replacement) {
        return text.split(item).join(replacement);
    },

    "readTextBox": function() {
        var text = document.getElementById("bitsInput").value;
        text = BitsToFloat.replaceAll(text, " ", "");

        if (text == "") {
            text = 0;
        }
        for (var i=0; i<text.length; i++) {
            var c = text.charAt(i);
            if (c != "0" && c != "1") {
                return null;
            }
        }

        if (isNaN(parseInt(text))) {
            return null;
        }
        
        if (text.length > 32) {
            return null;
        }

        while (text.length < 32) {
            text = "0" + text;
        }

        return text;
    },

    "extractParts": function(num) {
        var mantissa = "";
        var exponent = "";
        var sign = "";

        sign = num.charAt(0) == "1"? "1":"0";

        for (var i=1; i<=8; i++) {
            exponent += num.charAt(i);
        }
        for (var i=9; i<=31; i++) {
            mantissa += num.charAt(i);
            if (i == 15 || i == 23) {
                mantissa += " ";
            }
        }

        return {"isSpecialCase": exponent == "11111111", "sign":sign, "mantissa":mantissa, "exponent":exponent};
    },

    "calcAnswer": function(parts) {
        var sign = parts.sign == "0"? "":"-";
        document.getElementById("signResult").innerHTML = parts.sign == "0"? "+ (positive)":"- (negative)";

        var exp = 0;
        var pow = 1;
        for (var i=7; i>=0; i--) {
            var bit = parts.exponent.charAt(i);
            exp += bit * pow;
            pow *= 2;
        }

		if (exp != 0) {
			// Normal Representation
			document.getElementById("expResult").innerHTML = exp + " - 127 = " + (exp-127);
		}
		else {
			// Denormal Representation
			document.getElementById("expResult").innerHTML = "-126 (denormal number)";
		}

        parts.mantissa = BitsToFloat.replaceAll(parts.mantissa, " ", "")
		
		if (exp != 0) {
			// Normal Representation
			var mantissa = "= 1";
			var numerator = 1;
		}
		else {
			var mantissa = "";
			var numerator = 0;
		}
		
        var denominator = 1;
        var power = -1;
        var currentDenominator = 1;

        for (var i=0; i<=22; i++) {
            var bit = parts.mantissa.charAt(i);
            currentDenominator *= 2;
            if (bit == 1) {
                mantissa += " + 2<span class='superscript'>" + power + "</span>";

                var numFactor = currentDenominator / denominator;
                numerator = (numerator * numFactor) + 1;
                denominator = currentDenominator;
            }
            power -= 1;
        }

        if (exp != 0 && mantissa == "= 1") {
			// Normal number, nothing added to mantissa
            mantissa += " + 0";
        }
		else if (exp == 0 && mantissa == "") {
			// Denormal number, nothing added to mantissa
			mantissa = "= 0 (denormal number)";
		}
		else if (exp == 0) {
			// Denormal number, things added to mantissa
			mantissa = "= " + mantissa.substring(3) + " (denormal number)";
		}

        mantissa += "<br>= " + numerator + " / " + denominator;
        mantissa += "<br>= " + (numerator / denominator);

        document.getElementById("mantissaResult").innerHTML = mantissa;

        var answer = "";
        if (sign == "-") {
            answer += "(-1) x ";
        }
        else {
            answer += "(+1) x ";
        }

		if (exp != 0) {
			// Normal Number
			exp -= 127;
		}
		else {
			// Denormal Number
			exp = -126;
		}
        
        answer += "(" + numerator + " / " + denominator + ") x 2<span class='superscript'>" + exp + "</span>";
        
        if (sign == "-") {
            numerator *= -1;
        }
        if (exp < 0) {
            denominator *= Math.pow(2,-exp); 
        }
        else {
            numerator *= Math.pow(2,exp);
        }

        answer += "<br> = " + numerator + " / " + denominator;
        answer += "<br> = " + numerator / denominator

        document.getElementById("finalCalc").innerHTML = answer;
    },

    "calcSpecialCase": function(parts) {
        document.getElementById("expResult").innerHTML += " (special case)";

        if (parts.mantissa == "00000000000000000000000") {
            document.getElementById("mantissaResult").innerHTML = "= 0 (indicates infinity)";
            if (parts.sign == "0") {
                document.getElementById("finalCalc").innerHTML = "+&infin; (positive infinity)";
            }
            else {
                document.getElementById("finalCalc").innerHTML = "-&infin; (negative infinity)";
            }
        }
        else {
            document.getElementById("mantissaResult").innerHTML = "&ne; 0 (indicates NaN)<br>";
            
            var msb = parts.mantissa.charAt(0);
            if (msb == "1") {
                document.getElementById("mantissaResult").innerHTML += "MSB = 1 (indicates a quiet NaN)";
            }
            else {
                document.getElementById("mantissaResult").innerHTML += "MSB = 0 (indicates a signalling NaN)";
            }

            document.getElementById("finalCalc").innerHTML = "NaN (Not a Number)";
        }
    },

    "go": function() {
        var num = BitsToFloat.readTextBox();
        if (num == null) {
            document.getElementById("bitsToNumResult").style.display = "none";
            document.getElementById("bitsToNumError").style.display = "block";
            return;
        }
        else {
            document.getElementById("bitsToNumResult").style.display = "block";
            document.getElementById("bitsToNumError").style.display = "none";
        }

        var parts = BitsToFloat.extractParts(num);
        document.getElementById("signBitSpan").innerHTML = parts.sign;
        document.getElementById("expBitsSpan").innerHTML = parts.exponent;
        document.getElementById("mantissaBitsSpan").innerHTML = parts.mantissa;

        BitsToFloat.calcAnswer(parts);
        if (parts.isSpecialCase) {
            BitsToFloat.calcSpecialCase(parts);
        }
    }
};