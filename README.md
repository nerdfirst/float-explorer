# About this Code

The code in this repository is part of a tutorial by **0612 TV** entitled **Floating Point Numbers**, hosted on YouTube.

[![Click to Watch](https://img.youtube.com/vi/gc1Nl3mmCuY/0.jpg)](https://www.youtube.com/watch?v=gc1Nl3mmCuY "Click to Watch")

# Try it!

For the live version of this code, visit the NERDfirst website: [Floating Point Explorer](https://resources.nerdfirst.net/float)

# License

This project is licensed under the **Apache License 2.0**. For more details, please refer to LICENSE.txt within this repository.